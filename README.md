# UKI API

You must install [npm](https://nodejs.org/en/download/) first to run this program. After the installation finish, you can follow the instruction below.

## MongoDB

This server use MongoDB for data storage. Before you start the project, please install [MongoDB](https://www.mongodb.com/download-center) first.

## Project setup

`npm install` for install all the modules that require and listed in package.json.


## Run the server

`npm start` to run the server in `port: 3000`.

