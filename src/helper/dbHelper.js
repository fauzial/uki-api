const { MongoClient } = require('mongodb')
const dbConfig = require('../../config/api.config')

const DBConnections = {
    connect: async (callBack) => {
        let url = dbConfig.DB.URL + ':' + dbConfig.DB.PORT + '/' + dbConfig.DB.NAME
        await MongoClient.connect(url, { useNewUrlParser: true }, (err,db)=>{
            if(!err){
                //SET CONNECTION TO GLOBAL SO THE CONNECTION CAN BE ACESSED ANYWARE
                global.db = db.db(dbConfig.DB.NAME)
            }
            callBack(err, db)
        })
    }
}

module.exports = DBConnections