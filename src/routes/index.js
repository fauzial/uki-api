const basicController = require('./basic/basicController')
const employeeController = require('./employee/employeeController')

module.exports = (server) => {
    server.get('/', basicController)
    server.get('/api/employees', employeeController.readAllEmployee)
    server.get('/api/employees/:id', employeeController.readEployeeById)
    server.post('/api/employees', employeeController.createEmployee)
    server.put('/api/employees/:id', employeeController.editEmployee)
    server.del('/api/employees/:id', employeeController.deleteEmployee)
}